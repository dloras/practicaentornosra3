// Toda clase debe estar en un paquete
package ejercicio01;
//Una �nica clase o interface cuyo nombre es identico al nombre del fichero sin la extesi�n
import java.util.Scanner;
/**
 * @author fvaldeon
 * @since 31-01-2018
 **/
//El nombre de la clase debe ser en mayuscula
public class Ejercicio01 {
	// Los nombres de constantes todo en mayusculas
	// El nombre tiene que decir algo, CAD no dice nada
	final static String SALUDO = "Bienvenido al programa";
	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		// Los nombres de constantes todo en mayusculas
		System.out.println(SALUDO); 
		System.out.println("Introduce tu dni");
		String dni = lector.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = lector.nextLine();
		// Una sola declaracion por l�nea
		// Los espacios en blanco mejoran la legabilidad
		// No tiene que haber variables de una letra
		int numeroA = 7; 
		int numeroB = 16;
		int numeroC = 25;
		// Los espacios en blanco mejoran la legabilidad
		// El corchete inicial empieza en la misma l�nea
		if( numeroA > numeroB || numeroC % 5 != 0 && (numeroC * 3 - 1) > numeroB / numeroC) {
			System.out.println("Se cumple la condici�n");
		}
		// Los espacios en blanco mejoran la legabilidad
		// Se ponen parentesis para mejorar la claridad de las operaciones
		numeroC = numeroA + (numeroB * numeroC) + (numeroB / numeroA);
		// Usar un nombre de la variable que identifique lo que contiene
		// Los arrays se pueden inicializar en bloque
		// Los corchetes van seguidos de la variable "String[]"
		String[] arrayDiasSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"}; 
		mostrarDiasSemana(arrayDiasSemana);
		// Se debe de cerrar el scanner
		lector.close(); 
	}
	
	static void mostrarDiasSemana(String[] vectorDeStrings)
	{
		// Los espacios en blanco mejoran la legabilidad
		// El 7 es un magic number, hay que poner vectorDeStrings.lenght
		for(int dia = 0; dia < vectorDeStrings.length; dia++)
		{
			// La l�nea no puede superar 100 caracteres
			System.out.println("El dia de la semana en el que te encuentras [" + (dia + 1) + "-7] es el dia: " 
					+ vectorDeStrings[dia]); 
		}
	}
	
}