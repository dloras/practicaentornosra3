package ejercicio04;

import java.io.File;
import java.util.Scanner;

public class Ejercicio04 {
	
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	static int fallos;
	static String palabraSecreta;
	static Scanner input = new Scanner(System.in);
	static char[][] caracteresPalabra = new char[2][];

	public static void main(String[] args) {
		cargarFichero();
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		String caracteresElegidos = "";
		boolean acertado;
		System.out.println("Acierta la palabra");
		do {
			System.out.println("####################################");
			mostrarPalabra();
			caracteresElegidos = pedirCaracter(input, caracteresElegidos);
			contadorFallos(caracteresElegidos, caracteresPalabra);
			mostrarAhorcado();
			acertado = juegoTerminado(palabraSecreta, caracteresPalabra);
		} while (!acertado && fallos < FALLOS);
		input.close();
	}

	private static void cargarFichero() {
		File fich = new File("src\\palabras.txt");
		Scanner inputFichero = null;
		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

	private static void mostrarPalabra() {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

	private static boolean juegoTerminado(String palabraSecreta, char[][] caracteresPalabra) {
		boolean acertado;
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}

	private static String pedirCaracter(Scanner input, String caracteresElegidos) {
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static void contadorFallos(String caracteresElegidos, char[][] caracteresPalabra) {
		fallos = 0;
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
	}

	private static void mostrarAhorcado() {
		switch (fallos) {
			case 1:
				System.out.println("     ___");
				break;
			case 2:
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 3:
				System.out.println("  ____ ");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 4:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 5:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 6:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println("     ___");
				break;
			case 7:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println(" A   ___");
				break;
		}
	}

}