// Toda clase debe estar en un paquete
package ejercicio02;
//Una �nica clase o interface cuyo nombre es identico al nombre del fichero sin la extesi�n
import java.util.Scanner;
/**
 * @author fvaldeon
 * @since 31-01-2018
 **/
public class Ejercicio02 {
	// el nombre del lector no puede ser una letra
	static Scanner lector;
	// Magic number
	static int CANTIDAD_MAXIMA_ALUMNOS = 10;
	// El corchete inicial del metodo debe de estar al final de la primera linea
	public static void main(String[] args) {
		lector = new Scanner(System.in);
		int arrayNotas[] = new int[10];
		// Se declara al instante y debe llamarse i
		// En la condicion poner .lengh en vez de 10
		for(int i = 0; i < arrayNotas.length; i++)
		{
			System.out.println("Introduce nota media de alumno");
			// Se debe de leer un int y no un string
			// El nombre de la variable debe ser clarificador
			arrayNotas[i] = lector.nextInt();
		}	
		// Falta el ")" del syso
		System.out.println("El resultado es: " + recorrer_array(arrayNotas));
		lector.close();
	}
	// El corchete inicial del metodo debe ir al final de la linea
	static double recorrer_array(int[] vector) {
		// El nomobre de la variable debe ser claro
		double sumaNotas  = 0;
		// Los espacios en blanco mejoran la legabilidad
		// La variable del bucle debe ser i
		// En la condicion poner .lengh en vez de 10
		// El corchete incial del bucle debe estar al final de la linea
		for(int i = 0; i < vector.length; i++) {
			sumaNotas = sumaNotas + vector[i];
		}
		return (sumaNotas / CANTIDAD_MAXIMA_ALUMNOS);
	}
// Falta el "}" para cerrar la clase
}
	