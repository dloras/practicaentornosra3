package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {
	
	static String password = "";
	static int longitud;
	static int opcion;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		mostrarMenu();
		pedirDatos(scanner);
		switch (opcion) {
			case 1:
				generarCaracteres();
				break;
			case 2:
				generarNumeros();
				break;
			case 3:
				generarLetrasEspeciales();
				break;
			case 4:
				generarLetrasNumerosEspeciales();
				break;
		}
		System.out.println(password);
		scanner.close();
	}

	private static void mostrarMenu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}
	
	private static void pedirDatos(Scanner scanner) {
		System.out.println("Introduce la longitud de la cadena: ");
		longitud = scanner.nextInt();
		System.out.println("Elige tipo de password: ");
		opcion = scanner.nextInt();
	}
	
	private static void generarCaracteres() {
		for (int i = 0; i < longitud; i++) {
			password += caracterAleatorio();
		}
	}
	
	private static char caracterAleatorio() {
		return (char) ((Math.random() * 26) + 65);
	}
	
	private static void generarNumeros() {
		for (int i = 0; i < longitud; i++) {
			password += numeroAleatorio();
		}	
	}
	
	private static int numeroAleatorio() {
		return ((int)(Math.random() * 10));
	}
	
	private static void generarLetrasEspeciales() {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 2);
			if (n == 1) {
				password += caracterAleatorio();
			} else {
				password += especialAleatorio();
			}
		}
	}
	
	private static char especialAleatorio() {
		return ((char)((Math.random() * 15) + 33));
	}
	
	private static void generarLetrasNumerosEspeciales() {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 3);
			if (n == 1) {
				password += caracterAleatorio();
			} else if (n == 2) {
				password += especialAleatorio();
			} else {
				password += numeroAleatorio();
			}
		}
	}
	
}